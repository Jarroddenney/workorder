﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Collections.Specialized;
using System.Web;
using System.Net;

namespace WorkORders
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SubmitaRequest : ContentPage
	{
		public SubmitaRequest ()
		{
			InitializeComponent ();
		}

        private void BtnSubmit(object sender, EventArgs e)
        {
            var date = System.DateTime.Now.ToString();
           
            //Create the Work Order
            NameValueCollection parameters = new NameValueCollection()
                {
                //{"Name", user.name  },
                //{"Phone Number", user.phonenumber  },
                //{"Email Address", user.emailaddress  },
                //{"Area", this.FindByName<Entry>("area").Text },
                //{"Department", user.department },
                //{"Request", this.FindByName<Entry>("request").Text }
                };
            var uri = new Uri("https://isd.ccpj.net/mobile_request.html?Name="+parameters[0]+"&PhoneNum="+parameters[1]+"&email="+parameters[2]+"&area="+parameters[3]+"&department="+parameters[4]+"&request="+parameters[5]);
            var client = new WebClient();
            client.UploadValues(uri, parameters);
        }
    }
}