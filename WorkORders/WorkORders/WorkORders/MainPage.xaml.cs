﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace WorkORders
{
	public partial class MainPage : MasterDetailPage
	{
		public MainPage()
		{
			InitializeComponent();
            Detail = new NavigationPage(new Home());
		}

        private void BtnViewAll(object sender, EventArgs e)
        {
            Detail = new NavigationPage(new ViewAllWorkOrders());
            IsPresented = false;
        }

        private void BtnSubmitaRequest(object sender, EventArgs e)
        {
            Detail = new NavigationPage(new SubmitaRequest());
            IsPresented = false;
        }

        private void BtnHome(object sender, EventArgs e)
        {
            Detail = new NavigationPage(new Home());
        }
    }
}
