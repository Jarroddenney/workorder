﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkORders
{
    class WorkOrder
    {
        public string name { get; set; }
        public string phoneNum { get; set; }
        public string email { get; set; }
        public string department { get; set; }
        public string status { get; set; }
        public string orderNum { get; set; }
        public DateTime RequestDate { get; set; }
        public string request { get; set; }
        public string area { get; set; }
    }
}
